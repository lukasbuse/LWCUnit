# ==============================================================================
#   LWCUnit - A lightweight C Unit testing Framework
#   Copyright (c) 2019 Lukas Buse
#   [Released under MIT License. Please refer to license.txt for details]
# ============================================================================== 

#-------------------------------------------------------------------------------
# LWC Unit file and function identifier strings
#-------------------------------------------------------------------------------

TEST_ABSTRACTION_MARKER := LWC_ABSTRACTIONFILE
TEST_FILE_MARKER := LWC_TESTFILE
TEST_FUNCTION_MARKER := LWC_TESTFUNCTION
TEST_INSERT_MARKER := INSERT_CONTENTS_HERE

#-------------------------------------------------------------------------------
# LWC Unit directories and file names
#-------------------------------------------------------------------------------
PROJECT_ROOT = $(subst $(notdir $(CURDIR)),,$(CURDIR))
OBJDIR := obj
TEST_DIRECTORY = $(PROJECT_ROOT)test/
PRODUCTION_DIRECTORY = $(PROJECT_ROOT)src/
GENERATED_DIRECTORY = $(CURDIR)/generated/
TEST_FILES := $(shell find $(TEST_DIRECTORY) -name '*.c')
PRODUCTION_FILES := $(shell find $(PRODUCTION_DIRECTORY) -name '*.c')
PRODUCTION_FILES := $(subst $(PRODUCTION_DIRECTORY),../src/,$(PRODUCTION_FILES))
TEST_FILE_PREPARATION = $(subst .c,,$(TEST_FILES))
TEMPLATE_C := $(CURDIR)/template/testCasesTemplate.c
TEMPLATE_H := $(CURDIR)/template/testCasesTemplate.h
TEST_CASES_C := $(CURDIR)/generated/testCases.c 
TEST_CASES_H := $(CURDIR)/generated/testCases.h
LWC_TESTBENCH := lwcunit_testbench

#-------------------------------------------------------------------------------
# Symbols, that can't be used explicitly
#-------------------------------------------------------------------------------
space := $(empty) $(empty)
CBL := {
CBR := }
PL := (
PR := )
QUOTES := "
SLASH := $(shell echo '/')
TEST_FUNCTION_NR := 0
STEP := 1
ONE := 1
FINNISH_TESTCASES_H := 0
TAB := $(empty)  $(empty)
BULLIT := $(empty) -$(empty)

#-------------------------------------------------------------------------------
# Standard rule
#-------------------------------------------------------------------------------
.PHONY: all
all: preperation build runtests 

.PHONY: preperation
preperation: setup_testcase_files $(TEST_FILE_PREPARATION) finish_testcase_files

# Create files testCases.c and testCases.h and copy first part of template Code into them
.PHONY: setup_testcase_files
setup_testcase_files:
	@ mkdir -p $(GENERATED_DIRECTORY)
	$(info )
	$(info -- PARSING TEST INFORMATION ----------------------------------------------------)
	@> $(TEST_CASES_C)
	$(eval TEST_INSERT_LINENUMBER = $(shell grep -n "$(TEST_INSERT_MARKER)" $(TEMPLATE_C)))
	$(eval TEST_INSERT_LINENUMBER = $(subst :,,$(TEST_INSERT_LINENUMBER)))
	$(eval TEST_INSERT_LINENUMBER = $(subst $(TEST_INSERT_MARKER),,$(TEST_INSERT_LINENUMBER)))
	@head -$$(($(TEST_INSERT_LINENUMBER)-$(ONE))) $(TEMPLATE_C)>> $(TEST_CASES_C)
	
	@> $(TEST_CASES_H)
	$(eval TEST_INSERT_LINENUMBER = $(shell grep -n "$(TEST_INSERT_MARKER)" $(TEMPLATE_H)))
	$(eval TEST_INSERT_LINENUMBER = $(subst :,,$(TEST_INSERT_LINENUMBER)))
	$(eval TEST_INSERT_LINENUMBER = $(subst $(TEST_INSERT_MARKER),,$(TEST_INSERT_LINENUMBER)))
	@head -$$(($(TEST_INSERT_LINENUMBER)-$(ONE))) $(TEMPLATE_H)>> $(TEST_CASES_H)
	$(info found files:)

# Search for Testfiles with valid Header
.PHONY: $(TEST_FILE_PREPARATION) 
$(TEST_FILE_PREPARATION) :
	$(eval TEXT_MARKER_RESULTS = $(shell grep '$(TEST_FILE_MARKER)' $@.c))
	$(eval TEST_MARKER_FOUND = $(findstring $(TEST_FILE_MARKER),$(TEXT_MARKER_RESULTS)))

	$(eval ABSTRACTION_MARKER_RESULTS = $(shell grep '$(TEST_ABSTRACTION_MARKER)' $@.c))
	$(eval ABSTRACTION_MARKER_FOUND = $(findstring $(TEST_ABSTRACTION_MARKER),$(ABSTRACTION_MARKER_RESULTS)))

#If abstraction header found:
#Log abstractionfile
	$(if $(ABSTRACTION_MARKER_FOUND), \
		$(info )\
		$(eval ABSTRACTION_FILES_C += $@.c)\
		$(info $(BULLIT) ABSTRACTIONFILE: $(subst $(TEST_DIRECTORY),,$@).c)\
	,)

# If test header found:
# Log testfile and tastable file names	
	$(if $(TEST_MARKER_FOUND), \
		$(info )\
		$(info $(BULLIT) TESTFILE: $(subst $(TEST_DIRECTORY),,$@).c)\
		$(eval TESTABLE_C := $(subst $(QUOTES),,$(subst $(PR),,$(subst $(TEST_FILE_MARKER)$(PL),,$(TEXT_MARKER_RESULTS)))))\
		$(eval TESTABLES_C += $(TESTABLE_C))\
		$(info $(TAB)$(BULLIT) refers to: $(TESTABLE_C))\
		$(eval VALID_TEST_FILES_C += $@.c)\
	,)

# If test header found:
# Look for testfunctions and format their declaration
	$(if $(TEST_MARKER_FOUND),\
		$(eval TEST_FUNCTIONS_TEMP = $(subst $(CBL),;,$(TEST_FUNCTIONS_TEMP)))\
		$(eval TEST_FUNCTIONS_TEMP = $(shell grep  -A1 $(TEST_FUNCTION_MARKER) $@.c | grep -E -v $(TEST_FUNCTION_MARKER)))\
		$(eval TEST_FUNCTIONS_TEMP = $(subst --,,$(TEST_FUNCTIONS_TEMP)))\
		$(eval TEST_FUNCTION_NAMES_TEMP = $(subst $(CBL),,$(TEST_FUNCTIONS_TEMP)))\
		$(eval TEST_FUNCTIONS_TEMP = $(subst $(CBL),;,$(TEST_FUNCTIONS_TEMP)))\
		$(eval TEST_FUNCTIONS_TEMP = $(subst testResultType$(space),testResultType_,$(TEST_FUNCTIONS_TEMP)))\
		$(eval TEST_FUNCTION_NAMES_TEMP = $(subst $(PL)void$(PR),,$(TEST_FUNCTION_NAMES_TEMP)))\
		$(eval TEST_FUNCTION_NAMES_TEMP = $(subst testResultType,,$(TEST_FUNCTION_NAMES_TEMP)))\
	,)

# If test header found:
# Write testfunctionpointer to jump table in testCases.c and declarations to testCases.h
	$(if $(TEST_MARKER_FOUND),\
		$(info $(TAB)$(BULLIT) Tests:)\
		$(foreach function,$(TEST_FUNCTION_NAMES_TEMP),\
			$(info $(TAB)$(TAB)$(TAB) $(function))\
			$(shell echo '        $(CBL).functionPtr=$(function), .functionName="$(function)", .fileName="$(subst $(TEST_DIRECTORY),,$@).c"$(CBR),' >> $(TEST_CASES_C))\
			$(eval TEST_FUNCTION_NR = $(shell echo $$(($(TEST_FUNCTION_NR) + $(STEP)))))\
		)\
		$(foreach fkt,$(TEST_FUNCTIONS_TEMP),\
			$(shell echo '$(subst _,$(space),$(fkt))' >> $(TEST_CASES_H))\
		)\
	,)

# Copy remaining template code into testCases.c and testCases.h	
.PHONY: finish_testcase_files
finish_testcase_files :
	$(eval TESTABLES_C = $(sort $(TESTABLES_C)))
	$(eval PRODUCTION_FILES := $(filter-out $(TESTABLES_C),$(PRODUCTION_FILES)))
	$(eval PRODUCTION_FILES_O := $(subst .c,.o,$(PRODUCTION_FILES)))
	$(info $(TESTABLES_C))
	$(info $(PRODUCTION_FILES_O))
# testCases.c:
	$(eval NUMBER_OF_LINES_IN_TEMPLATE := $(shell wc -l < $(TEMPLATE_C)))
	$(eval TAIL := $(shell echo $$(($(NUMBER_OF_LINES_IN_TEMPLATE) - $(TEST_INSERT_LINENUMBER) +$(ONE)))))
	$(shell head -$(NUMBER_OF_LINES_IN_TEMPLATE) $(TEMPLATE_C)| tail -$(TAIL) >> $(TEST_CASES_C)) 

# testCases.h:
	$(shell echo \#define NUMBER_OF_TESTFUNCTIONS $(TEST_FUNCTION_NR) >> $(TEST_CASES_H))
	
	$(eval NUMBER_OF_LINES_IN_TEMPLATE := $(shell wc -l < $(TEMPLATE_H)))
	$(eval TAIL := $(shell echo $$(($(NUMBER_OF_LINES_IN_TEMPLATE) - $(TEST_INSERT_LINENUMBER)))))
	$(shell head -$(NUMBER_OF_LINES_IN_TEMPLATE) $(TEMPLATE_H)| tail -$(TAIL) >> $(TEST_CASES_H))

	$(eval ABSTRACTION_FILES_O := $(subst .c,.o,$(ABSTRACTION_FILES_C)))
	$(eval ABSTRACTION_FILES_O := $(subst $(TEST_DIRECTORY),,$(ABSTRACTION_FILES_O)))

	$(eval VALID_TEST_FILES_O := $(subst .c,.o,$(VALID_TEST_FILES_C)))
	$(eval VALID_TEST_FILES_O := $(subst $(TEST_DIRECTORY),,$(VALID_TEST_FILES_O)))

	$(eval TESTABLES_O := $(subst .c,.o,$(TESTABLES_C)))
	$(eval TESTABLES_BARE_O = )
	$(foreach full_path,$(TESTABLES_O),\
		$(eval TESTABLES_BARE_O += $(lastword $(subst $(SLASH),$(space),$(full_path))))\
	)

.PHONY: build 
build: preperation
	$(info )
	$(info -- BUILDING TESTS --------------------------------------------------------------)
	$(info )
	@$(MAKE) buildfiles \
	'VALID_TEST_FILES_O=$(VALID_TEST_FILES_O)' \
	'TESTABLES_O=$(TESTABLES_O)' \
	'TESTABLES_BARE_O=$(TESTABLES_BARE_O)' \
	'ABSTRACTION_FILES_O=$(ABSTRACTION_FILES_O)' \
	'PRODUCTION_FILES_O=$(PRODUCTION_FILES_O)' \


.PHONY: buildfiles
buildfiles: $(LWC_TESTBENCH) 
	$(info )

$(LWC_TESTBENCH): main.o testCases.o $(VALID_TEST_FILES_O) $(TESTABLES_O) $(ABSTRACTION_FILES_O) $(PRODUCTION_FILES_O)
	$(info $(TAB)linking $@)
	$(eval PRODUCTION_FILES_BARE_O := $(subst ../src/,,$(PRODUCTION_FILES_O)))
	@gcc -g -o $(LWC_TESTBENCH) main.o testCases.o $(VALID_TEST_FILES_O) $(TESTABLES_BARE_O) $(ABSTRACTION_FILES_O) $(PRODUCTION_FILES_BARE_O) -Wall

main.o: lwcunit.h $(TEST_CASES_H) testCases.o
	$(info $(TAB)building $@)
	@gcc -g -c main.c -Wall

testCases.o: generated/testCases.c $(TEST_CASES_H) $(VALID_TEST_FILES_O) lwcunit.h
	$(info $(TAB)building $@)
	@gcc -g -c generated/testCases.c -Wall

$(VALID_TEST_FILES_O): $(TEST_DIRECTORY)$(subst .o,.c,$@) $(TEST_CASES_H) $(TESTABLES_O)
	$(info $(TAB)building $@)
	@gcc -g -c "$(TEST_DIRECTORY)$(subst .o,.c,$@)" -Wall

$(TESTABLES_O): $(subst .o,.c,$@) $(subst .o,.h,$@) $(PRODUCTION_FILES_O) $(ABSTRACTION_FILES_O) 
	$(info $(TAB)building $@)
	@gcc -g -c "$(subst .o,.c,$@)" -Wall -D LWC_UNITTEST

$(PRODUCTION_FILES_O): $(subst .o,.c,$@)
	$(info $(TAB)building $@)
	@gcc -g -c "$(subst .o,.c,$@)" -Wall

$(ABSTRACTION_FILES_O): $(TEST_DIRECTORY)$(subst .o,.c,$@)
	$(info $(TAB)building $@)
	@gcc -g -c "$(TEST_DIRECTORY)$(subst .o,.c,$@)" -Wall

.PHONY: runtests
runtests: build
	$(info )
	$(info -- RUN TESTS -------------------------------------------------------------------)
	@./$(LWC_TESTBENCH)

.PHONY: clean
clean:
	@rm -r *.o $(CURDIR)/generated/* lwcunit_testbench make.exe.stackdump -f
