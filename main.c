/* =========================================================================
    LWCUnit - A lightweight C Unit testing Framework
    Copyright (c) 2019 Lukas Buse
    [Released under MIT License. Please refer to license.txt for details]
============================================================================ */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "lwcunit.h"
#include "generated/testCases.h"

static int AreNotEqual(char* str1, char* str2);

int main (void){

    int errorCode = 0;
    char previousFileName[LWC_STRING_LENGTH] = "";
    const char successTab[] = "    ";
    const char failTab[] = "  X ";
    uint8_t i = 0;

    do{
        jumpTableReturnType result;
        result = test(i++);
        errorCode = result.errorCode;

        if(result.errorCode > -2 && AreNotEqual(result.fileName, previousFileName)){
            printf("\nTESTFILE %s:\n", result.fileName);
            strcpy(previousFileName, result.fileName);
        }
        
        switch(result.errorCode){
            case LWC_TEST_SUCCESSFUL:
                printf("%s SUCCESS %s: %s \n", successTab ,result.functionName, result.returnMessage);
                break;
            case LWC_TEST_FAILED:
                printf("%s FAILED  %s: %s \n" , failTab,result.functionName, result.returnMessage);
                break;
        }
    }
    while(errorCode > -2);

    printf("\nTestrun finished. Exiting. \n\n");

    return 0;
}

static inline int AreNotEqual(char* str1, char* str2){
    return strcmp(str1,str2);
}