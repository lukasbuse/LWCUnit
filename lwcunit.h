/* =========================================================================
    LWCUnit - A lightweight C Unit testing Framework
    Copyright (c) 2019 Lukas Buse
    [Released under MIT License. Please refer to license.txt for details]
============================================================================ */

#ifndef LWCUNIT_H
#define LWCUNIT_H

#include <string.h>

#define LWC_UNITTEST                1
#define INSERT_CONTENTS_HERE
#define LWC_TESTFUNCTION
#define LWC_TESTFILE(x)
#define LWC_ABSTRACTIONFILE

#define LWC_TEST_FAILED           (-1)
#define LWC_TEST_SUCCESSFUL         0

#define LWC_STRING_LENGTH           80

typedef struct {
    char returnMessage[LWC_STRING_LENGTH]; 
    int errorCode;
    } testResultType;

typedef struct {
    testResultType (*functionPtr)(void);
    char functionName[LWC_STRING_LENGTH]; 
    char fileName[LWC_STRING_LENGTH];
    } functionInfoType;

typedef struct {
    char returnMessage[LWC_STRING_LENGTH]; 
    int errorCode; 
    char functionName[LWC_STRING_LENGTH]; 
    char fileName[LWC_STRING_LENGTH];
    }jumpTableReturnType;

#define AssertAreEqual(a,b,errorMessage) do{\
    if((a) != (b)){\
        testResultType returnVal;\
        strcpy(returnVal.returnMessage, errorMessage);\
        returnVal.errorCode = LWC_TEST_FAILED;\
        return returnVal;\
    }\
}while(0)

#define AssertIsTrue(a,errorMessage) do{\
    if(!(a)){\
        testResultType returnVal;\
        strcpy(returnVal.returnMessage, errorMessage);\
        returnVal.errorCode = LWC_TEST_FAILED;\
        return returnVal;\
    }\
}while(0)

#define AssertIsFalse(a,errorMessage) do{\
    if((a)){\
        testResultType returnVal;\
        strcpy(returnVal.returnMessage, errorMessage);\
        returnVal.errorCode = LWC_TEST_FAILED;\
        return returnVal;\
    }\
}while(0)

#define TEST_DONE() do{\
    testResultType returnVal;\
    strcpy(returnVal.returnMessage, "");\
    returnVal.errorCode = LWC_TEST_SUCCESSFUL;\
    return returnVal;\
}while(0)

#endif