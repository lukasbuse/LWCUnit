/* =========================================================================
    LWCUnit - A lightweight C Unit testing Framework
    Copyright (c) 2019 Lukas Buse
    [Released under MIT License. Please refer to license.txt for details]
============================================================================ 

    This file was generated. All manual changes will be overwritten! 
    
============================================================================ */


#ifndef TEST_CASES_TEMPLATE_H
#define TEST_CASES_TEMPLATE_H

#include <stdint.h>
#include "../lwcunit.h"

jumpTableReturnType test(uint8_t const jumpIndex);

INSERT_CONTENTS_HERE

#endif
