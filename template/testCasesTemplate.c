/* =========================================================================
    LWCUnit - A lightweight C Unit testing Framework
    Copyright (c) 2019 Lukas Buse
    [Released under MIT License. Please refer to license.txt for details]
============================================================================ 

    This file was generated. All manual changes will be overwritten! 
    
============================================================================ */

#include <stdint.h>
#include <string.h>
#include "../lwcunit.h"
#include "testCases.h"

jumpTableReturnType test(uint8_t const jumpIndex){

    // Jump table with additional function Information
    static functionInfoType functionInfo[] = {
        INSERT_CONTENTS_HERE
        };

    if (jumpIndex < sizeof(functionInfo)/sizeof(functionInfoType)){
        // Call the function specified by jumpIndex 
        testResultType testReturnVal =  functionInfo[jumpIndex].functionPtr();

        // Get function Info specified by jumpIndex
        jumpTableReturnType returnVal;
        strcpy(returnVal.returnMessage, testReturnVal.returnMessage);
        strcpy(returnVal.functionName, functionInfo[jumpIndex].functionName);
        strcpy(returnVal.fileName, functionInfo[jumpIndex].fileName);
        returnVal.errorCode = testReturnVal.errorCode;

        return returnVal;
    }
    else{
        // Error return value;
        jumpTableReturnType ErrorReturnVal = {
            .returnMessage = "Error: Jump index out of bounds.",
            .errorCode = -2, 
            .functionName = "",
            .fileName = ""
            };

        return ErrorReturnVal;
    }

}
